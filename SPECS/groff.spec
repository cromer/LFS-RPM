#TARBALL:	http://ftp.gnu.org/gnu/groff/groff-1.22.4.tar.gz
#MD5SUM:	08fb04335e2f5e73f23ea4c3adbf0c5f;SOURCES/groff-1.22.4.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Groff package contains programs for processing and formatting text.
Name:		groff
Version:	1.22.4
Release:	2
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/groff/%{name}-%{version}.tar.gz
Provides:	perl(File::HomeDir) = %{version}
Provides:	perl(main_subs.pl) = %{version}
Provides:	perl(man.pl) = %{version}
Provides:	perl(oop_fh.pl) = %{version}
Provides:	perl(subs.pl) = %{version}
Requires:	filesystem
%description
The Groff package contains programs for processing and formatting text.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	PAGE=letter ./configure --prefix=%{_prefix}
	make
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
	%{_mandir}/man5/*
	%{_mandir}/man7/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.22.4-2
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 1.22.4-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.22.3-1
-	Initial build.	First version
