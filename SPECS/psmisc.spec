#TARBALL:	https://sourceforge.net/projects/psmisc/files/psmisc/psmisc-23.2.tar.xz
#MD5SUM:	0524258861f00be1a02d27d39d8e5e62;SOURCES/psmisc-23.2.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Psmisc package contains programs for displaying information about running processes.
Name:		psmisc
Version:	23.2
Release:	2
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	https://sourceforge.net/projects/psmisc/files/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Psmisc package contains programs for displaying information about running processes.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/bin
	mv -v %{buildroot}%{_bindir}/fuser   %{buildroot}/bin
	mv -v %{buildroot}%{_bindir}/killall %{buildroot}/bin
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 23.2-2
*	Fri Apr 05 2019 baho-utot <baho-utot@columbus.rr.com> 23.2-1
-	Update for LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 23.1-1
-	Initial build.	First version
