#MD5SUM:	74127d907539428043addc03b7cb5730;SOURCES/linux-firmware.tar.gz
#-----------------------------------------------------------------------------
Summary:	Firmware for various hardware for linux
Name:		linux-firmware
Version:	1.0
Release:	2
License:	Any
URL:		Any
Group:		LFS/Base
Source0:	%{name}.tar.gz
Requires:	filesystem
%description
Firmware for various hardware for linux
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{name}
%build

%install
	make %{?_smp_mflags} DESTDIR="%{buildroot}" FIRMWAREDIR=/lib/firmware install
%files
	%defattr(-,root,root)
	/lib/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.0-2
*	Sat May 30 2020 Chris Cromer <chris@cromer.cl> 1.0-1
-	Initial build
