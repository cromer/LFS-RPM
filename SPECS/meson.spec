#TARBALL:	https://github.com/mesonbuild/meson/releases/download/0.53.1/meson-0.53.1.tar.gz
#MD5SUM:	9bf73f7b5a2426a7c8674a809bb8cae2;SOURCES/meson-0.53.1.tar.gz
#-----------------------------------------------------------------------------
Summary:	Meson is an open source build system
Name:		meson
Version:	0.53.1
Release:	1
License:	Any
URL:		Any
Group:		LFS/Base
Source0:	https://github.com/mesonbuild/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
Meson is an open source build system meant to be both extremely fast, and, even more importantly, as user friendly as possible.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	python3 setup.py build
%install
	install -vdm 755 %{buildroot}/usr/lib/python3.8/site-packages/
	python3 setup.py install --root="%{buildroot}" --optimize=1 --skip-build 
#-----------------------------------------------------------------------------
#	Copy license/copying file 
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/meson.1.gz
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 0.53.1-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 0.49.2-1
-	LFS-8.4
*	Wed Jul 25 2018 baho-utot <baho-utot@columbus.rr.com> 0.44.0-1
-	Initial build.	First version
