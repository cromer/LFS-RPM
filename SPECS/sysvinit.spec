#TARBALL:	http://download.savannah.gnu.org/releases/sysvinit/sysvinit-2.96.tar.xz
#MD5SUM:	48cebffebf2a96ab09bec14bf9976016;SOURCES/sysvinit-2.96.tar.xz
#TARBALL:	http://www.linuxfromscratch.org/patches/lfs/8.4/sysvinit-2.96-consolidated-1.patch
#MD5SUM:	4900322141d493e74020c9cf437b2cdc;SOURCES/sysvinit-2.96-consolidated-1.patch
#-----------------------------------------------------------------------------
Summary:	Controls the start up, running and shutdown of the system
Name:		sysvinit
Version:	2.96
Release:	1
License:	GPLv2
URL:		http://savannah.nongnu.org/projects/sysvinit
Group:		LFS/Base
Source0:	http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.xz
Patch0:		%{name}-%{version}-consolidated-1.patch
Requires:	filesystem
%description
Contains programs for controlling the start up, running and
shutdown of the system
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%patch0 -p1
%build
	make VERBOSE=1  %{?_smp_mflags}
%install
	make ROOT=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYRIGHT %{buildroot}/usr/share/licenses/%{name}/LICENSE
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man5/*
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.96-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 2.93-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.88dsf-1
-	Initial build.	First version
