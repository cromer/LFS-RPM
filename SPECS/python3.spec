#TARBALL:	https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tar.xz
#MD5SUM:	b3fb85fd479c0bf950c626ef80cacb57;SOURCES/Python-3.8.1.tar.xz
#TARBALL:	https://docs.python.org/ftp/python/doc/3.8.1/python-3.8.1-docs-html.tar.bz2
#MD5SUM:	edc8c97f9680373fcc1dd952f0ea7fcc;SOURCES/python-3.8.1-docs-html.tar.bz2
#-----------------------------------------------------------------------------
Summary:	The Python 3 package contains the Python development environment.
Name:		python3
Version:	3.8.1
Release:	1
License:	Any
URL:		Any
Group:		LFS/Base
Source0:	Python-%{VERSION}.tar.xz
Source1:	python-%{VERSION}-docs-html.tar.bz2
Requires:	filesystem
%description
The Python 3 package contains the Python development environment.
This is useful for object-oriented programming, writing scripts,
prototyping large programs or developing entire applications.
#-----------------------------------------------------------------------------
%prep
cd %{_builddir}
%setup -q -n "Python-%{VERSION}"
%setup -q -T -D -a 1 -n Python-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--enable-shared \
		--with-system-expat \
		--with-system-ffi \
		--with-ensurepip=yes
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	chmod -v 755 %{buildroot}/usr/lib/libpython3.8.so
	chmod -v 755 %{buildroot}/usr/lib/libpython3.so
	ln -sfv pip3.8 %{buildroot}/usr/bin/pip3
	install -v -dm755 %{buildroot}/usr/share/doc/python-%{version}/html
	cp -var python-%{version}-docs-html/* %{buildroot}/usr/share/doc/python-%{version}/html
	ln -sfv python3 %{buildroot}/usr/bin/python
	install -v -dm755 %{buildroot}/usr/local/bin
	ln -sfv python3 %{buildroot}/usr/local/bin/python
#-----------------------------------------------------------------------------
#	Copy license/copying file 
	install -D -m644 LICENSE %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 3.8.1-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> Python-3.7.2-1
-	LFS-8.4
*	Fri Jul 17 2018 baho-utot <baho-utot@columbus.rr.com> Python-3.6.4-1
-	Initial build.	First version
