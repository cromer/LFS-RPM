#TARBALL:	https://www.kernel.org/pub/linux/kernel/v5.x/linux-5.5.3.tar.xz
#MD5SUM:	3ea50025d8c679a327cf2fc225d81a46;SOURCES/linux-5.5.3.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Linux package contains the Linux kernel.
Name:		linux
Version:	5.5.3
Release:	1
License:	GPLv2
URL:		https://www.kernel.org
Group:		LFS/Base
Source0:	https://www.kernel.org/pub/linux/kernel/v5.x/%{name}-%{version}.tar.xz
Source1:	config-%{VERSION}
Requires:	filesystem
Requires:	mkinitramfs
%description
The Linux package contains the Linux kernel.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	make mrproper
#	make defconfig
#	make allmodconfig
	cp %{SOURCE1} .config
#	make oldconfig
	make olddefconfig
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} INSTALL_MOD_PATH=%{buildroot} modules_install
	install -vdm 755 %{buildroot}/boot
	cp -v arch/x86/boot/bzImage %{buildroot}/boot/vmlinuz-%{version}
	cp -v System.map %{buildroot}/boot/System.map-%{version}
	cp -v .config %{buildroot}/boot/config-%{version}
	install -d %{buildroot}%{_docdir}/%{NAME}-%{VERSION}
	cp -r Documentation/* %{buildroot}%{_docdir}/%{NAME}-%{version}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}-%{VERSION}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#-----------------------------------------------------------------------------
%post
		pushd /boot
		touch initrd.img-%{VERSION}
		rm initrd.img-%{VERSION}
		mkinitramfs %{VERSION}
		popd
%postun
		pushd /boot
		touch initrd.img-%{VERSION}
		rm initrd.img-%{VERSION}
		popd
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 5.5.3-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 4.15.3-1
-	Initial build.	First version
