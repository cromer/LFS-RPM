#TARBALL:	https://prdownloads.sourceforge.net/expat/expat-2.2.9.tar.xz
#MD5SUM:	d2384fa607223447e713e1b9bd272376;SOURCES/expat-2.2.9.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Expat package contains a stream oriented C library for parsing XML.
Name:		expat
Version:	2.2.9
Release:	1
License:	Other
URL:		Any
Group:		LFS/Base
Source0:	http://prdownloads.sourceforge.net/expat/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Expat package contains a stream oriented C library for parsing XML.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
	sed -i 's|usr/bin/env |bin/|' run.sh.in
%build
	./configure \
		--prefix=%{_prefix} \
		--disable-static \
		--docdir=%{_docdir}/%{name}-%{version}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -v -dm755 %{buildroot}%{_docdir}/%{name}-%{version}
	install -v -m644 doc/*.{html,png,css} %{buildroot}%{_docdir}/%{name}-%{version}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.2.9-1
*	Fri Apr 05 2019 baho-utot <baho-utot@columbus.rr.com> 2.2.6-1
-	Update for LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.2.5-1
-	Initial build.	First version
