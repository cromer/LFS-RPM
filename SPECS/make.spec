#TARBALL:	http://ftp.gnu.org/gnu/make/make-4.3.tar.gz
#MD5SUM:	fc7a67ea86ace13195b0bce683fd4469;SOURCES/make-4.3.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Make package contains a program for compiling packages.
Name:		make
Version:	4.3
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/make/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Make package contains a program for compiling packages.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure --prefix=%{_prefix}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 4.3-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 4.2.1-1
-	Initial build.	First version
