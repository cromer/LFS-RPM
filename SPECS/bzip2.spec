#TARBALL:	http://anduin.linuxfromscratch.org/LFS/bzip2-1.0.8.tar.gz
#MD5SUM:	67e051268d0c475ea773822f7500d0e5;SOURCES/bzip2-1.0.8.tar.gz
#TARBALL:	http://www.linuxfromscratch.org/patches/lfs/9.1/bzip2-1.0.8-install_docs-1.patch
#MD5SUM:	6a5ac7e89b791aae556de0f745916f7f;SOURCES/bzip2-1.0.8-install_docs-1.patch
#-----------------------------------------------------------------------------
Summary:	The Bzip2 package contains programs for compressing and decompressing files
Name:		bzip2
Version:	1.0.8
Release:	1
License:	Other
URL:		Any
Group:		LFS/Base
Source0:	http://www.%{name}.org/%{version}/%{name}-%{version}.tar.gz
Patch0:		http://www.linuxfromscratch.org/patches/lfs/9.1/%{name}-%{version}-install_docs-1.patch
Requires:	filesystem
%description
The Bzip2 package contains programs for compressing and decompressing files
Compressing text files with bzip2 yields a much better compression percentage
than with the traditional gzip.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%patch0 -p1 
	sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
	sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
%build
	make -f Makefile-libbz2_so
	make clean
	make
%install
	make PREFIX=%{buildroot}/%{_prefix} install
	install -vdm 755 %{buildroot}/bin
	install -vdm 755 %{buildroot}/lib
	install -vdm 755 %{buildroot}%{_libdir}
	install -vdm 755 %{buildroot}%{_bindir}
	cp -v bzip2-shared %{buildroot}/bin/bzip2
	cp -av libbz2.so* %{buildroot}/lib
	ln -sv ../../lib/libbz2.so.1.0 %{buildroot}%{_libdir}/libbz2.so
	rm -v %{buildroot}/%{_bindir}/{bunzip2,bzcat,bzip2}
	ln -sv bzip2 %{buildroot}/bin/bunzip2
	ln -sv bzip2 %{buildroot}/bin/bzcat
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 LICENSE %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl>
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.0.6-1
-	Initial build.	First version
