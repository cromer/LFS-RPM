#TARBALL:	https://www.kernel.org/pub/linux/docs/man-pages/man-pages-5.05.tar.xz
#MD5SUM:	da25a4f8dfed0a34453c90153b98752d;SOURCES/man-pages-5.05.tar.xz
#-----------------------------------------------------------------------------
Summary:	Man pages
Name:		man-pages
Version:	5.05
Release:	1
License:	GPLv2
URL:		http://www.kernel.org/doc/man-pages
Group:		LFS/Base
Source0:	http://www.kernel.org/pub/linux/docs/man-pages/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Man-pages package contains over 2,200 man pages.
#-----------------------------------------------------------------------------
%prep
%setup -q
%build
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 man-pages-%{version}.Announce %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man2/*
	%{_mandir}/man3/*
	%{_mandir}/man4/*
	%{_mandir}/man5/*
	%{_mandir}/man6/*
	%{_mandir}/man7/*
	%{_mandir}/man8/*
	%{_datarootdir}/licenses/man-pages/LICENSE
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 5.05-1
*	Fri Mar 15 2019 baho-utot <baho-utot@columbus.rr.com> 4.16-1
*	Mon Mar 19 2018 baho-utot <baho-utot@columbus.rr.com> 4.15-1
*	Tue Dec 19 2017 baho-utot <baho-utot@columbus.rr.com> 4.12-1
*	Sat Mar 22 2014 baho-utot <baho-utot@columbus.rr.com> 3.59-1
*	Sat Aug 24 2013 baho-utot <baho-utot@columbus.rr.com> 3.53-1
*	Fri May 10 2013 baho-utot <baho-utot@columbus.rr.com> 3.51-1
*	Sun Mar 24 2013 baho-utot <baho-utot@columbus.rr.com> 3.50-1
*	Wed Jan 30 2013 baho-utot <baho-utot@columbus.rr.com> 3.42-1
