#TARBALL:	https://www.kernel.org/pub/linux/kernel/v5.x/linux-5.5.3.tar.xz
#MD5SUM:	3ea50025d8c679a327cf2fc225d81a46;SOURCES/linux-5.5.3.tar.xz
#-----------------------------------------------------------------------------
Summary:	Linux API header files
Name:		linux-api-headers
Version:	5.5.3
Release:	1
License:	GPLv2
URL:		http://www.kernel.org/
Group:		LFS/Base
Source0:	http://www.kernel.org/pub/linux/kernel/v5.x/linux-%{version}.tar.xz
Requires:	filesystem
%description
The Linux API Headers expose the kernel's API for use by Glibc.
#-----------------------------------------------------------------------------
%prep
%setup -q -n linux-%{version}
%build
	make mrproper
%install
	make headers
	find usr/include -name '.*' -delete
	rm usr/include/Makefile
	install -vdm 755 %{buildroot}/%{_includedir}
	cp -rv usr/include/* %{buildroot}/%{_includedir}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 5.5.3-1
*	Fri Mar 15 2019 baho-utot <baho-utot@columbus.rr.com> 4.20.12-1
*	Thu Mar 15 2018 baho-utot <baho-utot@columbus.rr.com> 4.15.3-1
*	Wed Jan 31 2018 baho-utot <baho-utot@columbus.rr.com> 4.9.67-2
*	Tue Dec 19 2017 baho-utot <baho-utot@columbus.rr.com> 4.9.67-1
-	update to version 4.9.67
*	Sat Mar 22 2014 baho-utot <baho-utot@columbus.rr.com> 3.13.3-1
*	Sat Aug 31 2013 baho-utot <baho-utot@columbus.rr.com> 3.10.10-1
*	Sat Aug 24 2013 baho-utot <baho-utot@columbus.rr.com> 3.10.9-1
*	Thu Jun 27 2013 baho-utot <baho-utot@columbus.rr.com> 3.9.7-1
*	Wed May 15 2013 baho-utot <baho-utot@columbus.rr.com> 3.9.2-1
*	Sat May 11 2013 baho-utot <baho-utot@columbus.rr.com> 3.9.1-1
*	Fri May 10 2013 baho-utot <baho-utot@columbus.rr.com> 3.9-1
*	Mon Apr 1 2013 baho-utot <baho-utot@columbus.rr.com> 3.8.5-1
*	Sun Mar 24 2013 baho-utot <baho-utot@columbus.rr.com> 3.8.3-1
*	Sun Mar 24 2013 baho-utot <baho-utot@columbus.rr.com> 3.8.1-1
*	Wed Jan 30 2013 baho-utot <baho-utot@columbus.rr.com> 3.5.2-1
-	initial version
