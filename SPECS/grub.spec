#	%%define	_optflags -march=x86-64 -mtune=generic -O2 -pipe
#TARBALL:	http://ftp.gnu.org/gnu/grub/grub-2.04.tar.xz
#MD5SUM:	5aaca6713b47ca2456d8324a58755ac7;SOURCES/grub-2.04.tar.xz
#-----------------------------------------------------------------------------
Summary:	The GRUB package contains the GRand Unified Bootloader.
Name:		grub
Version:	2.04
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/grub/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The GRUB package contains the GRand Unified Bootloader.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--sbindir=/sbin \
		--sysconfdir=%{_sysconfdir} \
		--disable-efiemu \
		--disable-werror
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/usr/share/bash-completion/completions
	mv -v %{buildroot}/etc/bash_completion.d/grub %{buildroot}/usr/share/bash-completion/completions
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.04-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.02-1
-	Initial build.	First version
