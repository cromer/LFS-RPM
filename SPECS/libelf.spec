#TARBALL:	https://sourceware.org/ftp/elfutils/0.178/elfutils-0.178.tar.bz2
#MD5SUM:	5480d0b7174446aba13a6adde107287f;SOURCES/elfutils-0.178.tar.bz2
#-----------------------------------------------------------------------------
Summary:	The elfutils package contains a set of utilities and libraries for handling ELF files
Name:		libelf
Version:	0.178
Release:	1
License:	GPLv3
URL:		https://sourceware.org/ftp/elfutils
Group:		LFS/BASE
Source0:	https://sourceware.org/ftp/elfutils/%{version}/elfutils-%{version}.tar.bz2
Requires:	filesystem
%description
The elfutils package contains a set of utilities and libraries for handling ELF
(Executable and Linkable Format) files.
#-----------------------------------------------------------------------------
%prep
%setup -q -n elfutils-%{version}
%build
	./configure \
		--prefix=%{_prefix} \
		--disable-debuginfod
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} -C libelf install
	install -vDm644 config/libelf.pc %{buildroot}/%{_libdir}/pkgconfig/libelf.pc
	rm %{buildroot}/%{_libdir}/libelf.a
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 0.178-1
*	Fri Apr 05 2019 baho-utot <baho-utot@columbus.rr.com> 0.176-1
-	Update for LFS-8.4
*	Mon Jan 01 2018 baho-utot <baho-utot@columbus.rr.com> 0.170-1
-	LFS-8.1
