#TARBALL:	https://dev.gentoo.org/~blueness/eudev/eudev-3.2.9.tar.gz
#MD5SUM:	dedfb1964f6098fe9320de827957331f;SOURCES/eudev-3.2.9.tar.gz
#TARBALL:	http://anduin.linuxfromscratch.org/LFS/udev-lfs-20171102.tar.xz
#MD5SUM:	27cd82f9a61422e186b9d6759ddf1634;SOURCES/udev-lfs-20171102.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Eudev package contains programs for dynamic creation of device nodes.
Name:		eudev
Version:	3.2.9
Release:	1
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	https://dev.gentoo.org/~blueness/%{name}/%{name}-%{version}.tar.gz
Source1:	http://anduin.linuxfromscratch.org/LFS/udev-lfs-20171102.tar.xz
Requires:	filesystem
%description
The Eudev package contains programs for dynamic creation of device nodes.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%setup -q -T -D -a 1
%build
	./configure \
		--prefix=%{_prefix} \
		--bindir=/sbin \
		--sbindir=/sbin \
		--libdir=%{_libdir} \
		--sysconfdir=%{_sysconfdir} \
		--libexecdir=/lib \
		--with-rootprefix= \
		--with-rootlibdir=/lib \
		--enable-manpages \
		--disable-static \
		--config-cache
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	make -f udev-lfs-20171102/Makefile.lfs DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list	
%post
	LD_LIBRARY_PATH=/tools/lib udevadm hwdb --update
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man5/*
	%{_mandir}/man7/*
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 3.2.9-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 3.2.7-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 3.2.5-1
-	Initial build.	First version
