#TARBALL:	http://download.savannah.gnu.org/releases/acl/acl-2.2.53.tar.gz
#MD5SUM:	007aabf1dbb550bcddde52a244cd1070;SOURCES/acl-2.2.53.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Acl package contains utilities to administer Access Control Lists
Name:		acl
Version:	2.2.53
Release:	2
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Acl package contains utilities to administer Access Control Lists, which are
used to define more fine-grained discretionary access rights for files and directories.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{name}-%{version}
%build
	./configure \
		--prefix=%{_prefix} \
		--bindir=/bin \
		--disable-static \
		--libexecdir=%{_libdir} \
		--docdir=%{_docdir}/%{name}-%{version}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/lib
	install -vdm 755 %{buildroot}/usr/lib
	mv -v %{buildroot}/usr/lib/libacl.so.* %{buildroot}/lib
	ln -sfv ../../lib/$(readlink %{buildroot}/usr/lib/libacl.so) %{buildroot}/usr/lib/libacl.so
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 doc/COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man3/*
	%{_mandir}/man5/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.2.53-2
*	Wed Apr 03 2019 baho-utot <baho-utot@columbus.rr.com> 2.2.53-1
-	update
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.2.52-1
-	Initial build.	First version
