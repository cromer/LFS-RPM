#TARBALL:	http://zlib.net/zlib-1.2.11.tar.xz
#MD5SUM:	85adef240c5f370b308da8c938951a68;SOURCES/zlib-1.2.11.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Zlib package contains compression and decompression routines used by some programs.
Name:		zlib
Version:	1.2.11
Release:	2
License:	Other
URL:		http://zlib.net
Group:		LFS/Base
Source0:	http://zlib.net/%{name}-%{version}.tar.xz
Provides:	pkgconfig(zlib) = %{version}
Requires:	filesystem
%description
The Zlib package contains compression and decompression routines used by some programs.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure --prefix=%{_prefix}
	make  %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/lib
	install -vdm 755 %{buildroot}/usr/lib
	mv -v %{buildroot}/usr/lib/libz.so.* %{buildroot}/lib
	ln -sfv ../../lib/$(readlink %{buildroot}/usr/lib/libz.so) %{buildroot}/usr/lib/libz.so
#-----------------------------------------------------------------------------
	#	Copy license/copying file
	install -vDm644 README %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
	#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%_mandir/man3/zlib.3.gz
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 1.2.11-2
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.2.11-1
-	Initial build.	First version
