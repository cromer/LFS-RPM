#TARBALL:	http://sourceforge.net/projects/procps-ng/files/Production/procps-ng-3.3.15.tar.xz
#MD5SUM:	2b0717a7cb474b3d6dfdeedfbad2eccc;SOURCES/procps-ng-3.3.15.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Procps-ng package contains programs for monitoring processes.
Name:		procps-ng
Version:	3.3.15
Release:	2
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	http://sourceforge.net/projects/%{name}/files/Production/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Procps-ng package contains programs for monitoring processes.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--exec-prefix= \
		--libdir=%{_libdir} \
		--docdir=%{_docdir}/%{NAME}-%{VERSION} \
		--disable-static \
		--disable-kill
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/lib
	mv -v %{buildroot}%{_libdir}/libprocps.so.* %{buildroot}/lib
	ln -sfv ../../lib/$(readlink %{buildroot}%{_libdir}/libprocps.so) %{buildroot}%{_libdir}/libprocps.so
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man3/*
	%{_mandir}/man5/*
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 3.3.15-2
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 3.3.15-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 3.3.12-1
-	Initial build.	First version
