#TARBALL:	http://ftp.gnu.org/gnu/findutils/findutils-4.7.0.tar.xz
#MD5SUM:	731356dec4b1109b812fecfddfead6b2;SOURCES/findutils-4.7.0.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Findutils package contains programs to find files.
Name:		findutils
Version:	4.7.0
Release:	1
License:	Any
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Findutils package contains programs to find files. These programs
are provided to recursively search through a directory tree and to
create, maintain, and search a database (often faster than the recursive
find, but unreliable if the database has not been recently updated).
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--localstatedir=%{_localstatedir}/lib/locate
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/bin
	mv -v %{buildroot}%{_bindir}/find %{buildroot}/bin
	sed -i 's|find:=${BINDIR}|find:=/bin|' %{buildroot}%{_bindir}/updatedb
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list	
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
	%{_mandir}/man5/*
	%dir %{_localstatedir}/lib/locate
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 4.7.0-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 4.6.0-1
-	Initial build.	First version
