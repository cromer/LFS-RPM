#TARBALL:	http://download.savannah.gnu.org/releases/man-db/man-db-2.9.0.tar.xz
#MD5SUM:	897576a19ecbef376a916485608cd790;SOURCES/man-db-2.9.0.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Man-DB package contains programs for finding and viewing man pages.
Name:		man-db
Version:	2.9.0
Release:	1
License:	Other
URL:		Any
Group:		LFS/Base
Source0:	http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Man-DB package contains programs for finding and viewing man pages.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--docdir=%{_docdir}/%{NAME}-%{VERSION} \
		--sysconfdir=%{_sysconfdir} \
		--disable-setuid \
		--enable-cache-owner=bin \
		--with-browser=%{_bindir}/lynx \
		--with-vgrind=%{_bindir}/vgrind \
		--with-grap=%{_bindir}/grap \
		--with-systemdtmpfilesdir= \
		--with-systemdsystemunitdir=
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 README %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/man\/es/d'  filelist.list	
	sed -i '/man\/it/d'  filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man5/*
	%{_mandir}/man8/*
	%{_mandir}/man8/*
	%{_mandir}/it/man1/*
	%{_mandir}/it/man5/*
	%{_mandir}/it/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.9.0-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.8.1-1
-	Initial build.	First version
