#TARBALL:	ftp://anduin.linuxfromscratch.org/BLFS/popt/popt-1.16.tar.gz
#MD5SUM:	3743beefa3dd6247a73f8f7a32c14c33;SOURCES/popt-1.16.tar.gz
#-----------------------------------------------------------------------------
Summary:	The popt package contains the popt libraries which are used by some programs parse command-line options.
Name:		popt
Version:	1.16
Release:	2
License:	GPL
URL:		ftp://anduin.linuxfromscratch.org/BLFS/%{name}/
Group:		BLFS/General_Libraries 
Source0:	ftp://anduin.linuxfromscratch.org/BLFS/%{name}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The popt package contains the popt libraries which are used by some programs parse command-line options.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{name}-%{version}
%build
	./configure \
		--prefix=%{_prefix} \
		--disable-static
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
	#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man3/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.16-2
*	Mon Jan 01 2018 baho-utot <baho-utot@columbus.rr.com> 1.16-1
-	LFS-8.1
