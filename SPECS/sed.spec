#TARBALL:	http://ftp.gnu.org/gnu/sed/sed-4.8.tar.xz
#MD5SUM:	6d906edfdb3202304059233f51f9a71d;SOURCES/sed-4.8.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Sed package contains a stream editor
Name:		sed
Version:	4.8
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Sed package contains a stream editor
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
	sed -i 's/usr/tools/' build-aux/help2man
	sed -i 's/testsuite.panic-tests.sh//' Makefile.in
%build
	./configure \
		--prefix=%{_prefix} \
		--bindir=/bin
	make %{?_smp_mflags}
	make %{?_smp_mflags} html
%install
	make DESTDIR=%{buildroot} install
	install -d -m755 %{buildroot}%{_docdir}/%{NAME}-%{VERSION}
	install -m644 doc/sed.html %{buildroot}%{_docdir}/%{NAME}-%{VERSION}
	rm -rf %{buildroot}/%{_infodir}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 4.8-1
*	Wed Apr 03 2019 baho-utot <baho-utot@columbus.rr.com> 4.7-1
-	Update
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 4.4-1
-	Initial build.	First version
