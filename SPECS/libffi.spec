#TARBALL:	ftp://sourceware.org/pub/libffi/libffi-3.3.tar.gz
#MD5SUM:	6313289e32f1d38a9df4770b014a2ca7;SOURCES/libffi-3.3.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Libffi library provides a portable, high level programming interface to various calling conventions.
Name:		libffi
Version:	3.3
Release:	1
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Libffi library provides a portable, high level programming interface to various calling conventions.
This allows a programmer to call any function specified by a call interface description at run time.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--disable-static
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 LICENSE %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man3/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 3.3-1
*	Fri Jul 17 2018 baho-utot <baho-utot@columbus.rr.com> 3.2.1-1
-	Initial build.	First version
