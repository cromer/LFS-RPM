#TARBALL:	https://github.com/facebook/zstd/releases/download/v1.4.4/zstd-1.4.4.tar.gz
#MD5SUM:	487f7ee1562dee7c1c8adf85e2a63df9;SOURCES/zstd-1.4.4.tar.gz
#-----------------------------------------------------------------------------
Summary:	Zstandard - Fast real-time compression algorithm.
Name:		zstd
Version:	1.4.4
Release:	1
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	https://github.com/facebook/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
Zstandard is a real-time compression algorithm, providing high compression
ratios. It offers a very wide range of compression / speed trade-offs, while
being backed by a very fast decoder. 
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
	make %{?_smp_mflags}
%install
	make prefix=%{buildroot}/%{_prefix} install
	install -vdm 755 %{buildroot}/lib
    rm -v %{buildroot}/usr/lib/libzstd.a
    mv -v %{buildroot}/usr/lib/libzstd.so.* %{buildroot}/lib
    ln -sfv ../../lib/$(readlink %{buildroot}/usr/lib/libzstd.so) %{buildroot}/usr/lib/libzstd.so
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.4.4-1
-	Initial build.	First version
