#TARBALL:	http://ftp.gnu.org/gnu/patch/patch-2.7.6.tar.xz
#MD5SUM:	78ad9937e4caadcba1526ef1853730d5;SOURCES/patch-2.7.6.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Patch package contains a program for modifying or creating files.
Name:		patch
Version:	2.7.6
Release:	2
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Patch package contains a program for modifying or creating
files by applying a patch file typically created by the diff program.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure --prefix=%{_prefix}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#	%%{_infodir}/*
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.7.6-2
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.7.6-1
-	Initial build.	First version
