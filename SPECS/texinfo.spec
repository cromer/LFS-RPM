#TARBALL:	http://ftp.gnu.org/gnu/texinfo/texinfo-6.7.tar.xz
#MD5SUM:	d4c5d8cc84438c5993ec5163a59522a6;SOURCES/texinfo-6.7.tar.xz
%define __requires_exclude perl\\(Locale::gettext_xs\\)
#-----------------------------------------------------------------------------
Summary:	The Texinfo package contains programs for reading, writing, and converting info pages.
Name:		texinfo
Version:	6.7
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
Provides:	perl(Texinfo::ParserNonXS) = %{version}
%description
The Texinfo package contains programs for reading, writing, and converting info pages.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--disable-static
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	make DESTDIR=%{buildroot} TEXMF=/usr/share/texmf install-tex
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
	%{_mandir}/man5/*
%post
	pushd /usr/share/info
	rm -v dir
	for f in *;	do install-info $f dir 2>/dev/null; done
	popd
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 6.7-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 6.5-1
-	Initial build.	First version
