#TARBALL:	https://www.cpan.org/src/5.0/perl-5.30.1.tar.xz
#MD5SUM:	f399f3aaee90ddcff5eadd3bccdaacc0;SOURCES/perl-5.30.1.tar.xz
%define __requires_exclude perl\\((VMS|BSD::|Win32|Tk|Mac::|Your::Module::Here|unicore::Name|FCGI|Locale::Codes::.*(Code|Retired))
#|^perl\\(\s\\)
# the following suppresses dependency checks on all modules in /usr/lib/perl5/5.30.1 directories
%define __requires_exclude_from %{_libdir}/perl5
#-----------------------------------------------------------------------------
Summary:	The Perl package contains the Practical Extraction and Report Language.
Name:		perl
Version:	5.30.1
Release:	1
License:	GPLv1
URL:		Any
Group:		LFS/Base
Source0:	http://www.cpan.org/src/5.0/%{name}-%{version}.tar.xz
Provides:	perl = 1:5
Provides:	perl = 1:5.8.0
Provides:	perl = 0:5.008001
Provides:	perl = 0:5.009001
Requires:	filesystem
%description
The Perl package contains the Practical Extraction and Report Language.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	export BUILD_ZLIB=False
	export BUILD_BZIP2=0
	sh Configure -des -Dprefix=/usr \
		-Dvendorprefix=/usr \
		-Dman1dir=%{_mandir}/man1 \
		-Dman3dir=%{_mandir}/man3 \
		-Dpager="${_sbindir}/less -isR" \
		-Duseshrplib \
		-Dusethreads
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 Copying %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man3/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 5.30.1-1
*	Fri Apr 05 2019 baho-utot <baho-utot@columbus.rr.com> 5.28.1-1
-	Update for LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 5.26.1-1
-	Initial build.	First version
