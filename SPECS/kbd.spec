#TARBALL:	https://www.kernel.org/pub/linux/utils/kbd/kbd-2.2.0.tar.xz
#MD5SUM:	d1d7ae0b5fb875dc082731e09cd0c8bc;SOURCES/kbd-2.2.0.tar.xz
#TARBALL:	http://www.linuxfromscratch.org/patches/lfs/9.1/kbd-2.2.0-backspace-1.patch
#MD5SUM:	f75cca16a38da6caa7d52151f7136895;SOURCES/kbd-2.2.0-backspace-1.patch
#-----------------------------------------------------------------------------
Summary:	The Kbd package contains key-table files, console fonts, and keyboard utilities.
Name:		kbd
Version:	2.2.0
Release:	1
License:	Other
URL:		Any
Group:		LFS/Base
Source0:	https://www.kernel.org/pub/linux/utils/%{name}/%{name}-%{version}.tar.xz
Patch0:		http://www.linuxfromscratch.org/patches/lfs/9.1/kbd-2.2.0-backspace-1.patch
Requires:	filesystem
%description
The Kbd package contains key-table files, console fonts, and keyboard utilities.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%patch0 -p1
	sed -i 's/\(RESIZECONS_PROGS=\)yes/\1no/g' configure
	sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
%build
	PKG_CONFIG_PATH=/tools/lib/pkgconfig \
	./configure \
		--prefix=%{_prefix} \
		--disable-vlock
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}%{_docdir}/%{NAME}-%{VERSION}
	cp -R -v docs/doc/* %{buildroot}%{_docdir}/%{NAME}-%{VERSION}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#	%%{_infodir}/*
	%{_mandir}/man1/*
	%{_mandir}/man5/*
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.2.0-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.0.4-1
-	Initial build.	First version
