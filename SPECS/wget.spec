#TARBALL:	https://ftp.gnu.org/gnu/wget/wget-1.20.3.tar.gz
#MD5SUM:	db4e6dc7977cbddcd543b240079a4899;SOURCES/wget-1.20.3.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Wget package contains a utility useful for non-interactive downloading of files from the Web. 
Name:		wget
Version:	1.20.3
Release:	1
License:	Any
URL:		Any
Group:		LFS/Base
Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Wget package contains a utility useful for non-interactive downloading of files from the Web. 
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--sysconfdir=%{_sysconfdir} \
		--with-ssl=openssl \
		--disable-rpath
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file 
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
%post
	pushd /usr/share/info
	rm -v dir
	for f in *; do install-info $f dir 2>/dev/null; done
	popd
%postun
	pushd /usr/share/info
	rm -v dir
	for f in *; do install-info $f dir 2>/dev/null; done
	popd
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.20.3-1
*	Sun Apr 07 2019 baho-utot <baho-utot@columbus.rr.com> 1.20.1-1
-	BLFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.19.1-1
-	Initial build.	First version
