#TARBALL:	http://ftp.gnu.org/gnu/gettext/gettext-0.20.1.tar.xz
#MD5SUM:	9ed9e26ab613b668e0026222a9c23639;SOURCES/gettext-0.20.1.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Gettext package contains utilities for internationalization and localization.
Name:		gettext
Version:	0.20.1
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/gettext/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Gettext package contains utilities for internationalization and localization.
These allow programs to be compiled with NLS (Native Language Support), enabling
them to output messages in the user's native language.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--disable-static \
		--docdir=/usr/share/doc/%{NAME}-%{VERSION}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	chmod -v 0755 %{buildroot}%{_libdir}/preloadable_libintl.so
	rm -rf %{buildroot}%{_docdir}/%{NAME}-%{VERSION}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
	%{_mandir}/man3/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 0.20.1-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 0.19.8.1-1
-	Initial build.	First version
