#TARBALL:	https://www.kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-2.31.tar.xz
#MD5SUM:	52120c05dc797b01f5a7ae70f4335e96;SOURCES/libcap-2.31.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Libcap package implements the user-space interfaces to the POSIX 1003.1e
Name:		libcap
Version:	2.31
Release:	1
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Libcap package implements the user-space interfaces to the POSIX 1003.1e
capabilities available in Linux kernels. These capabilities are a partitioning
of the all powerful root privilege into a set of distinct privileges.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
	sed -i '/install.*STALIBNAME/d' libcap/Makefile
%build
	make lib=lib %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} lib=lib prefix=%{_prefix} install
	chmod -v 755 %{buildroot}%{_libdir}/libcap.so.%{version}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 License %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man3/*
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.31-1
*	Wed Apr 03 2019 baho-utot <baho-utot@columbus.rr.com> 2.26-1
-	Update
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.25-1
-	Initial build.	First version
