#TARBALL:	https://github.com/libcheck/check/releases/download/0.14.0/check-0.14.0.tar.gz
#MD5SUM:	270e82a445be6026040267a5e11cc94b;SOURCES/check-0.14.0.tar.gz
#-----------------------------------------------------------------------------
Summary:	Check is a unit testing framework for C.
Name:		check
Version:	0.14.0
Release:	1
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source:		https://github.com/libcheck/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
Check is a unit testing framework for C.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	sed -i '1 s/tools/usr/' ${RPM_BUILD_ROOT}/usr/bin/checkmk
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING.LESSER %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 0.14.0-1
*	Sat Jul 28 2018 baho-utot <baho-utot@columbus.rr.com> 0.12.0-1
-	Initial build.	First version
