#TARBALL:	https://www.kernel.org/pub/linux/utils/util-linux/v2.35/util-linux-2.35.1.tar.xz
#MD5SUM:	7f64882f631225f0295ca05080cee1bf;SOURCES/util-linux-2.35.1.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Util-linux package contains miscellaneous utility programs.
Name:		util-linux
Version:	2.35.1
Release:	1
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	https://www.kernel.org/pub/linux/utils/%{name}/v2.35/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Util-linux package contains miscellaneous utility programs.
Among them are utilities for handling file systems, consoles,
partitions, and messages.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		ADJTIME_PATH=/var/lib/hwclock/adjtime \
		--docdir=%{_docdir}/%{NAME}-%{VERSION} \
		--disable-chfn-chsh  \
		--disable-login \
		--disable-nologin \
		--disable-su \
		--disable-setpriv \
		--disable-runuser \
		--disable-pylibmount \
		--disable-static \
		--without-python \
		--without-systemd \
		--without-systemdsystemunitdir
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man3/*
	%{_mandir}/man5/*
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.35.1-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 2.33.1-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.31.1-1
-	Initial build.	First version
