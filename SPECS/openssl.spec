#TARBALL:	https://openssl.org/source/openssl-1.1.1d.tar.gz
#MD5SUM:	3be209000dbc7e1b95bcdf47980a3baa;SOURCES/openssl-1.1.1d.tar.gz
%define __requires_exclude perl\\(WWW::Curl::Easy)
#-----------------------------------------------------------------------------
Summary:	The OpenSSL package contains management tools and libraries relating to cryptography	
Name:		openssl
Version:	1.1.1d
Release:	1
License:	GPL
URL:		https://openssl.org/source
Group:		LFS/Base
Source0:	https://openssl.org/source/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The OpenSSL package contains management tools and libraries relating to cryptography.
These are useful for providing cryptographic functions to other packages, such as OpenSSH,
email applications and web browsers (for accessing HTTPS sites).
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{name}-%{version}
%build
	./config \
		--prefix=%{_prefix} \
		--openssldir=%{_sysconfdir}/ssl \
		--libdir=lib \
		shared \
		zlib-dynamic \
		enable-md2 # this is needed for rpm
	make %{?_smp_mflags}
%install
	sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
	make DESTDIR=%{buildroot} MANSUFFIX=ssl install
	mv -v %{buildroot}/usr/share/doc/%{name} %{buildroot}/usr/share/doc/%{name}-%{version}
	cp -vfr doc/* %{buildroot}/usr/share/doc/%{name}-%{version}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 LICENSE %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list	
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man3/*
	%{_mandir}/man5/*
	%{_mandir}/man7/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 1.1.1d-1
*	Sun Feb 03 2019 baho-utot <baho-utot@columbus.rr.com> 1.1.1a-1
-	LFS-8.4
*	Mon Jan 01 2018 baho-utot <baho-utot@columbus.rr.com> 1.1.0f-1
*	Fri Jul 17 2018 baho-utot <baho-utot@columbus.rr.com> 1.1.0g-1
-	LFS-8.1
