#TARBALL:	https://ftp.gnu.org/gnu/cpio/cpio-2.13.tar.bz2
#MD5SUM:	f3438e672e3fa273a7dc26339dd1eed6;SOURCES/cpio-2.13.tar.bz2
#-----------------------------------------------------------------------------
Summary:	The cpio package contains tools for archiving
Name:		cpio
Version:	2.13
Release:	1
License:	GPLv3
URL:		Any
Group:		BLFS/System_Utilities 
Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.bz2
Requires:	filesystem
%description
The cpio package contains tools for archiving
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--bindir=/bin \
		--enable-mt \
		--with-rmt=/usr/libexec/rmt
	make %{?_smp_mflags}
	makeinfo --html            -o doc/html      doc/cpio.texi
	makeinfo --html --no-split -o doc/cpio.html doc/cpio.texi
	makeinfo --plaintext       -o doc/cpio.txt  doc/cpio.texi
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%post
	pushd /usr/share/info
	rm -v dir
	for f in *; do install-info $f dir 2>/dev/null; done
	popd
%postun
	pushd /usr/share/info
	rm -v dir
	for f in *; do install-info $f dir 2>/dev/null; done
	popd
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 2.13-1
*	Wed Feb 14 2018 baho-utot <baho-utot@columbus.rr.com> 2.12-1
-	Initial build.	First version
