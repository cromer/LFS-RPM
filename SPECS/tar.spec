#TARBALL:	http://ftp.gnu.org/gnu/tar/tar-1.32.tar.xz
#MD5SUM:	83e38700a80a26e30b2df054e69956e5;SOURCES/tar-1.32.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Tar package contains an archiving program.
Name:		tar
Version:	1.32
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Tar package contains an archiving program.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
sed -i 's/abort.*/FALLTHROUGH;/' src/extract.c
%build
	FORCE_UNSAFE_CONFIGURE=1 \
	./configure \
		--prefix=%{_prefix} \
		--bindir=/bin
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	make -C doc DESTDIR=%{buildroot} install-html docdir=%{_docdir}/%{NAME}-%{VERSION}
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
	%{_mandir}/man8/*

#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.32-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 1.31-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.30-1
-	Initial build.	First version
