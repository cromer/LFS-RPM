%global __requires_exclude perl\\(getopts.pl\\)
%define __requires_exclude_from /usr/share/vim
#TARBALL:	ftp://ftp.vim.org/pub/vim/unix/vim-8.2.0190.tar.gz
#MD5SUM:	f5337b1170df90e644a636539a0313a3;SOURCES/vim-8.2.0190.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Vim package contains a powerful text editor.
Name:		vim
Version:	8.2.0190
Release:	1
License:	Charityware
URL:		Any
Group:		LFS/Base
Source0:	ftp://ftp.vim.org/pub/%{name}/unix/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Vim package contains a powerful text editor.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{name}-%{version}
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
%build
	./configure \
		--prefix=%{_prefix} \
		--localstatedir=/var/lib/vim \
		--with-x=no \
		--disable-gui
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755 %{buildroot}/usr/share/doc/
	ln -sv ../vim/vim82/doc %{buildroot}/usr/share/doc/%{name}-%{version}
	install -vdm 755 %{buildroot}/etc/vimrc
	cat > /etc/vimrc <<- "EOF"
		" Begin /etc/vimrc

		set nocompatible
		set backspace=2
		set mouse=r
		syntax on
		if (&term == "xterm") || (&term == "putty")
			set background=dark
		endif

		" End /etc/vimrc
	EOF
	rm -rf %{buildroot}/usr/share/info/dir
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 README.txt %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/man\/da/d'  filelist.list
	sed -i '/man\/da.ISO8859-1/d'  filelist.list
	sed -i '/man\/da.UTF-8/d'  filelist.list
	sed -i '/man\/de/d'  filelist.list
	sed -i '/man\/de.ISO8859-1/d'  filelist.list
	sed -i '/man\/de.UTF-8/d'  filelist.list
	sed -i '/man\/fr/d'  filelist.list
	sed -i '/man\/pl.ISO8859-2/d'  filelist.list
	sed -i '/man\/pl.UTF-8/d'  filelist.list
	sed -i '/man\/ru.UTF-8/d'  filelist.list
	sed -i '/man\/pl/d'  filelist.list
	sed -i '/man\/it.UTF-8/d'  filelist.list
	sed -i '/man\/ja/d'  filelist.list
	sed -i '/man\/it.ISO8859-1/d'  filelist.list
	sed -i '/man\/it/d'  filelist.list
	sed -i '/man\/fr.UTF-8/d'  filelist.list
	sed -i '/man\/ru.KOI8-R/d'  filelist.list
	sed -i '/man\/fr.ISO8859-1/d'  filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/da/man1/*
	%{_mandir}/da.ISO8859-1/man1/*
	%{_mandir}/da.UTF-8/man1/*
	%{_mandir}/de/man1/*
	%{_mandir}/de.ISO8859-1/man1/*
	%{_mandir}/de.UTF-8/man1/*
	%{_mandir}/fr.ISO8859-1/man1/*
	%{_mandir}/fr.UTF-8/man1/*
	%{_mandir}/fr/man1/*	
	%{_mandir}/it.ISO8859-1/man1/*
	%{_mandir}/it.UTF-8/man1/*
	%{_mandir}/it/man1/*
	%{_mandir}/ja/man1/*
	%{_mandir}/pl.ISO8859-2/man1/*
	%{_mandir}/pl.UTF-8/man1/*
	%{_mandir}/pl/man1/*
	%{_mandir}/ru.KOI8-R/man1/*
	%{_mandir}/ru.UTF-8/man1/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 8.2.0190-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 8.1-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 8.0.586-1
-	Initial build.	First version
