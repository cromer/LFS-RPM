#TARBALL:	http://www.linuxfromscratch.org/lfs/downloads/9.1/lfs-bootscripts-20191031.tar.xz
#MD5SUM:	e9249541960df505e4dfac0c32369372;SOURCES/lfs-bootscripts-20191031.tar.xz
#-----------------------------------------------------------------------------
Summary:	The lfs-bootscripts package contains a set of scripts to start/stop the LFS system at bootup/shutdown.
Name:		lfs-bootscripts
Version:	20191031
Release:	1
License:	None
URL:		http://www.linuxfromscratch.org
Group:		LFS/Base
Source0:	http://www.linuxfromscratch.org/lfs/downloads/9.1/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The lfs-bootscripts package contains a set of scripts to start/stop the LFS system
at bootup/shutdown. The configuration files and procedures needed to customize the
boot process are described in the following sections.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 20191031-1
*	Sun Apr 07 2019 baho-utot <baho-utot@columbus.rr.com> 20180820-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 20170626-1
-	Initial build.	First version
