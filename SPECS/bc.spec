#TARBALL:	http://ftp.gnu.org/gnu/bc/bc-2.5.3.tar.gz
#MD5SUM:	6582c6fbbae943fbfb8fe14a34feab57;SOURCES/bc-2.5.3.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Bc package contains an arbitrary precision numeric processing language
Name:		bc
Version:	2.5.3
Release:	1
License:	GPLv3
URL:		http://www.gnu.org
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Bc package contains an arbitrary precision numeric processing language
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	PREFIX=%{_prefix} CC=gcc CFLAGS="-std=c99" ./configure.sh -G -O3
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 LICENSE.md %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.5.3-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.07.1-1
-	Initial build.	First version
