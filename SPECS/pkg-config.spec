#TARBALL:	https://pkg-config.freedesktop.org/releases/pkg-config-0.29.2.tar.gz
#MD5SUM:	f6e931e319531b736fadc017f470e68a;SOURCES/pkg-config-0.29.2.tar.gz
#-----------------------------------------------------------------------------
Summary:	pkg-config package contains a tool for passing the include and library paths
Name:		pkg-config
Version:	0.29.2
Release:	2
License:	GPLv2
URL:		Any
Group:		LFS/Base
Source0:	https://pkg-config.freedesktop.org/releases/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The pkg-config package contains a tool for passing the include path and/or
library paths to build tools during the configure and make file execution.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
	./configure \
		--prefix=%{_prefix} \
		--with-internal-glib \
		--disable-host-tool \
		--docdir=%{_docdir}/%{NAME}-%{VERSION}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 0.29.2-2
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 0.29.2-1
-	Initial build.	First version
