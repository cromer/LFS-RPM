#TARBALL:	http://www.greenwoodsoftware.com/less/less-551.tar.gz
#MD5SUM:	4ad4408b06d7a6626a055cb453f36819;SOURCES/less-551.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Less package contains a text file viewer.
Name:		less
Version:	551
Release:	1
License:	Other
URL:		Any
Group:		LFS/Base
Source0:	http://www.greenwoodsoftware.com/less/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Less package contains a text file viewer.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	./configure \
		--prefix=%{_prefix} \
		--sysconfdir=%{_sysconfdir}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 LICENSE %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 551-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 530-1
-	Initial build.	First version
