#TARBALL:	ftp.rpm.org/releases/rpm-4.15.x/rpm-4.15.1.tar.bz2
#TARBALL:	http://download.oracle.com/berkeley-db/db-6.0.20.tar.gz
#MD5SUM:	ed72147451a5ed93b2a48e2f8f5413c3;SOURCES/rpm-4.15.1.tar.bz2
#MD5SUM:	b99454564d5b4479750567031d66fe24;SOURCES/db-5.3.28.tar.gz
#-----------------------------------------------------------------------------
Summary:	Package manager
Name:		rpm
Version:	4.15.1
Release:	1
License:	GPLv2
URL:		http://rpm.org
Group:		LFS/BASE
Source0:	http://ftp.rpm.org/releases/rpm-4.15.x/%{name}-%{version}.tar.bz2
Source1:	http://download.oracle.com/berkeley-db/db-5.3.28.tar.gz
Source2:	macros
Requires:	filesystem
%description
Package manager
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{name}-%{version}
%setup -q -T -D -a 1 -n %{name}-%{version}
sed -i 's/--srcdir=$db_dist/--srcdir=$db_dist --with-pic/' db3/configure
%build
	ln -vs db-5.3.28 db
	./configure \
		--prefix=%{_prefix} \
		--program-prefix= \
		--sysconfdir=%{_sysconfdir} \
		--with-crypto=openssl \
		--with-cap \
		--with-acl \
		--without-external-db \
		--without-archive \
		--without-lua \
		--disable-dependency-tracking \
		--disable-silent-rules \
		--disable-rpath \
		--disable-plugins \
		--disable-inhibit-plugin
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	install -vdm 755	%{buildroot}/%{_sysconfdir}/rpm
	install -vm  644	%{SOURCE2} %{buildroot}/%{_sysconfdir}/rpm
#-----------------------------------------------------------------------------
#	Copy license/copying file 
	install -D -m644 COPYING %{buildroot}%{_datarootdir}/licenses/%{name}-%{version}/COPYING
	install -D -m644 INSTALL %{buildroot}%{_datarootdir}/licenses/%{name}-%{version}/INSTALL
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
	sed -i '/man\/fr/d' filelist.list
	sed -i '/man\/pl/d' filelist.list
	sed -i '/man\/sk/d' filelist.list
	sed -i '/man\/ko/d' filelist.list
	sed -i '/man\/ja/d' filelist.list
	sed -i '/man\/ru/d' filelist.list	
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man1/*
	%{_mandir}/man8/*
	%{_mandir}/fr/man8/*
	%{_mandir}/ja/man8/*
	%{_mandir}/ko/man8/*
	%{_mandir}/pl/man1/*
	%{_mandir}/pl/man8/*
	%{_mandir}/ru/man8/*
	%{_mandir}/sk/man8/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 4.15.1-1
*	Wed Sep 26 2018 baho-utot <baho-utot@columbus.rr.com> 4.14.1-2
*	Sat Jul 28 2018 baho-utot <baho-utot@columbus.rr.com> 4.14.1-1
*	Sat Mar 10 2018 baho-utot <baho-utot@columbus.rr.com> 4.14.0-4
-	Added acl and cap Removed plugins and disabled python
*	Tue Feb 20 2018 baho-utot <baho-utot@columbus.rr.com> 4.14.0-3
-	Added python bindings for rpmlint
*	Mon Jan 01 2018 baho-utot <baho-utot@columbus.rr.com> 4.14.0-1
-	LFS-8.1
