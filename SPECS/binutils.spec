#TARBALL:	http://ftp.gnu.org/gnu/binutils/binutils-2.34.tar.xz
#MD5SUM:	664ec3a2df7805ed3464639aaae332d6;SOURCES/binutils-2.34.tar.xz
#-----------------------------------------------------------------------------
Summary:	The Binutils package contains a linker, an assembler, and other tools for handling object files
Name:		binutils
Version:	2.34
Release:	1
License:	GPLv3
URL:		http://ftp.gnu.org
Group:		LFS/Base
Source0:	http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Requires:	filesystem
%description
The Binutils package contains a linker, an assembler, and other tools for handling object files
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
	mkdir build
	cd build
	../configure \
		--prefix=%{_prefix} \
		--enable-gold \
		--enable-ld=default \
		--enable-plugins \
		--enable-shared \
		--disable-werror \
		--enable-64-bit-bfd \
		--with-system-zlib
	make %{?_smp_mflags} tooldir=/usr
%install
	cd build
	make DESTDIR=%{buildroot} tooldir=/usr install
	cd -
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING3 %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.34-1
*	Mon Mar 25 2019 baho-utot <baho-utot@columbus.rr.com> 2.32-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.30-1
-	Initial build.	First version
