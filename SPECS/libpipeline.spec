#TARBALL:	http://download.savannah.gnu.org/releases/libpipeline/libpipeline-1.5.2.tar.gz
#MD5SUM:	169de4cc1f6f7f7d430a5bed858b2fd3;SOURCES/libpipeline-1.5.2.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Libpipeline package contains a library for manipulating pipelines of subprocesses in a flexible and convenient way.
Name:		libpipeline
Version:	1.5.2
Release:	1
License:	GPLv3
URL:		Any
Group:		LFS/Base
Source0:	%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Libpipeline package contains a library for manipulating pipelines of subprocesses in a flexible and convenient way.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	PKG_CONFIG_PATH=/tools/lib/pkgconfig \
	./configure --prefix=%{_prefix}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
#	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
#	%%{_infodir}/*
	%{_mandir}/man3/*
#-----------------------------------------------------------------------------
%changelog
*   Mon Jun 01 2020 Chris Cromer <chris@cromer.cl> 1.5.2-1
*	Sat Apr 06 2019 baho-utot <baho-utot@columbus.rr.com> 1.5.1-1
-	LFS-8.4
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 1.5.0-1
-	Initial build.	First version
