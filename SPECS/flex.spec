#TARBALL:	https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz
#MD5SUM:	2882e3179748cc9f9c23ec593d6adc8d;SOURCES/flex-2.6.4.tar.gz
#-----------------------------------------------------------------------------
Summary:	The Flex package contains a utility for generating programs that recognize patterns in text.
Name:		flex
Version:	2.6.4
Release:	2
License:	BSD
URL:		Any
Group:		LFS/Base
Source0:	https://github.com/westes/flex/releases/download/v2.6.4/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The Flex package contains a utility for generating programs that recognize patterns in text.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
	sed -i "/math.h/a #include <malloc.h>" src/flexdef.h
%build
	HELP2MAN=/tools/bin/true \
	./configure \
		--prefix=%{_prefix} \
		--docdir=%{_docdir}/%{name}-%{version}
	make %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
	ln -sv flex %{buildroot}%{_bindir}/lex
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 COPYING %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	rm  %{buildroot}%{_infodir}/dir
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_infodir}/*
	%{_mandir}/man1/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.6.4-2
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.6.4-1
-	Initial build.	First version
