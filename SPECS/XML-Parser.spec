#TARBALL:	http://cpan.metacpan.org/authors/id/T/TO/TODDR/XML-Parser-2.46.tar.gz
#MD5SUM:	80bb18a8e6240fcf7ec2f7b57601c170;SOURCES/XML-Parser-2.46.tar.gz
#-----------------------------------------------------------------------------
Summary:	The XML::Parser module is a Perl interface to James Clark's XML parser, Expat.
Name:		XML-Parser
Version:	2.46
Release:	1
License:	Non-GPL
URL:		Any
Group:		LFS/Base
Source0:	http://cpan.metacpan.org/authors/id/T/TO/TODDR/%{name}-%{version}.tar.gz
Requires:	filesystem
%description
The XML::Parser module is a Perl interface to James Clark's XML parser, Expat.
#-----------------------------------------------------------------------------
%prep
%setup -q -n %{NAME}-%{VERSION}
%build
	perl Makefile.PL
	make  %{?_smp_mflags}
%install
	make DESTDIR=%{buildroot} install
#-----------------------------------------------------------------------------
#	Copy license/copying file
	install -D -m644 README %{buildroot}/usr/share/licenses/%{name}/LICENSE
#-----------------------------------------------------------------------------
#	Create file list
	find %{buildroot} -name '*.la' -delete
	find "${RPM_BUILD_ROOT}" -not -type d -print > filelist.list
	sed -i "s|^${RPM_BUILD_ROOT}||" filelist.list
	sed -i '/man\/man/d' filelist.list
	sed -i '/\/usr\/share\/info/d' filelist.list
#-----------------------------------------------------------------------------
%files -f filelist.list
	%defattr(-,root,root)
	%{_mandir}/man3/*
#-----------------------------------------------------------------------------
%changelog
*	Sun May 31 2020 Chris Cromer <chris@cromer.cl> 2.46-1
*	Tue Jan 09 2018 baho-utot <baho-utot@columbus.rr.com> 2.44-1
-	Initial build.	First version
